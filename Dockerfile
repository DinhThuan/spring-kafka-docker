FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE
COPY target/spring-kafka-docker-0.0.1-SNAPSHOT.jar spring-kafka-docker-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/spring-kafka-docker-0.0.1-SNAPSHOT.jar"]