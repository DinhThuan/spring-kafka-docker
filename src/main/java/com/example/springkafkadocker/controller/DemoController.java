package com.example.springkafkadocker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
    @GetMapping("hello")
    public String demo(){
        System.out.println("hello world");
        return "hello world 1";
    }
}
